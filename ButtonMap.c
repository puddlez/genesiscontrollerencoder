#include "USBKeyboard.h"

unsigned char ButtonMap[12];

void ConfigButtonMap_Genesis(void)
{
	ButtonMap[0] = KEY_UP;
	ButtonMap[1] = KEY_DOWN;
	ButtonMap[2] = KEY_LEFT;
	ButtonMap[3] = KEY_RIGHT;
	ButtonMap[4] = KEY_B;
	ButtonMap[5] = KEY_C;
	ButtonMap[6] = KEY_A;
	ButtonMap[7] = KEY_S;
	ButtonMap[8] = KEY_Z;
	ButtonMap[9] = KEY_Y;
	ButtonMap[10] = KEY_X;
	ButtonMap[11] = KEY_M;
}