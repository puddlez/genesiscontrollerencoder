@echo off
make
if %errorlevel% EQU 0 dfu-programmer atmega32u2 erase
if %errorlevel% EQU 0 dfu-programmer atmega32u2 flash output.hex
make clean