/*
Genesis Encoder v1.1
Sega Genesis Controller to USB Keyboard Encoder
20-OCT-2019

Written by Robert Lloyd
rl636018@gmail.com

Compatible with the following AVR microcontrollers:
atmega32u2
atmega32u4
at90usb162
at90usb646
at90usb1286

In Makefile, set MCU to device and F_CPU to clock speed
In Main.c, set CLKPR according to clock speed

To configure other devices, see device configuration section in UsbKeyboard.h

intended for compilation using avr-gcc, tested with v7.3.0
*/

#include <avr/io.h>
#include <util/delay.h>
#include "UsbKeyboard.c"
#include "ButtonMap.c"

void Debounce(void);
void UpdateKeys(void);

// input table
// pressed = low, not pressed = high
// cycle 0:	|1 |1 |St|A |0 |0 |Dn|Up|
// cycle 1:	|1 |1 |C |B |Rt|Lf|Dn|Up|
// cycle 2:	|1 |1 |St|A |0 |0 |Dn|Up|
// cycle 3:	|1 |1 |C |B |Rt|Lf|Dn|Up|
// cycle 4:	|1 |1 |St|A |0 |0 |0 |0 |
// cycle 5:	|1 |1 |C |B |Md|X |Y |Z |
// 6-button controllers report as shown
// 3-button controllers report all even cycles as 0, odd as 1

// button state variables
// low = pressed, high = not pressed
// [0]	|St|A |C |B |Rt|Lf|Dn|Up|
// [1]	|  |  |  |  |Md|X |Y |Z |
unsigned char RawInput[2] = { 0b11111111, 0b11111111 };	   // raw state of button input registers prior to debouncing
unsigned char ButtonState[2] = { 0b11111111, 0b11111111 }; // final button states after debouncing

unsigned char DebounceTimer[12] = { 0,0,0,0,0,0,0,0,0,0,0,0 }; // time remaining until new state will be accepted for each button
unsigned char DebouncePeriod = 7; // period that new inputs are ignored for a given button after a change in ButtonState

int main(void)
{
	CLKPR = 0x80, CLKPR = 0; // set clock prescale for 16 MHz clock
	
	// set I/O registers
	// -----------------
	// B0-7 unused
	// C0-1 unused
	// C2   select output
	// C3-6 unused
	// C7   trigger output for debugging
	// D0-5 data input
	// D6-7 unused
	DDRB = 0b00000000;
	DDRC = 0b10000100;
	DDRD = 0b11000000;
	
	// enable pull-up resistors for inputs and default outputs high
	PORTB = 0b11111111;
	PORTC = 0b11111111;
	PORTD = 0b11111111;
	
	// initialize USB communication
	InitializeUsb();
	
	// wait until USB communication finishes configuration
	while(!UsbConfigured());
	
	// configure ButtonMap for Genesis controller
	ConfigButtonMap_Genesis();
		
    // main input loop
    while (1)
    {
	    // long delay required for controller to reset
		//   should be adjusted so that main input loop lasts approx 2ms
	    _delay_ms(1.89);
		
	    // cycle 0 - select low (debugging trigger also low, only during this cycle)
	    PORTC = 0b01111011;
	    _delay_us(3);
		
	    // cycle 1 - select high
	    PORTC = 0b11111111;
	    _delay_us(3);
		
	    // cycle 2 - select low
	    PORTC = 0b11111011;
	    _delay_us(3);
		
	    // cycle 3 - select high
	    PORTC = 0b11111111;
	    _delay_us(3);
	    RawInput[0] = PIND;
		
	    // cycle 4 - select low
	    PORTC = 0b11111011;
	    _delay_us(2);
		
	    unsigned char input = PIND;
	    unsigned char controllerType; // 3 = 3-button controller, 6 = 6-button controller
		
	    if ((input & 0b00000011) == 0)
			controllerType = 6;
	    else
			controllerType = 3;
		
	    RawInput[0] = (RawInput[0] & 0b00111111) | ((input << 2) & 0b11000000);
		
	    // cycle 5 - select high
	    PORTC = 0b11111111;
	    _delay_us(2);
		
	    if (controllerType == 6)
			RawInput[1] = PIND | 0b11110000;
	    else
			RawInput[1] = 0b11111111;
		
	    // debounce RawInput and determine ButtonState
	    Debounce();
		
	    // update Keys currently pressed according to ButtonState and ButtonMap
	    UpdateKeys();
    }
}

// debounces RawInput and determines ButtonState
void Debounce()
{
	// update DebounceTimer for each button, and ButtonState if needed
	for (unsigned char currentButton = 0; currentButton < 12; currentButton++)
	{
		// if DebounceTimer has not reached 0, decrement DebounceTimer
		if (DebounceTimer[currentButton] != 0)
			DebounceTimer[currentButton]--;
		
		// if DebounceTimer has reached 0, update ButtonState
		if (DebounceTimer[currentButton] == 0) 
		{
			unsigned char buttonBit = currentButton;
			unsigned char buttonIndex = 0;
			
			// adjust buttonBit and buttonIndex to select position in ButtonState and RawInput
			while(buttonBit > 7)
			{
				buttonBit -= 8;
				buttonIndex++;
			}
			
			// if RawInput is not equal to ButtonState, update ButtonState and set DebounceTimer to DebouncePeriod
			if ((ButtonState[buttonIndex] & (1 << buttonBit)) != (RawInput[buttonIndex] & (1 << buttonBit)))
			{
				ButtonState[buttonIndex] = ((ButtonState[buttonIndex] & ~(1 << buttonBit)) | (RawInput[buttonIndex] & (1 << buttonBit)));
				
				DebounceTimer[currentButton] += DebouncePeriod;
			}
		}
	}
}

// updates Keys currently pressed according to ButtonState and ButtonMap
void UpdateKeys()
{
	// update Keys for each button according to ButtonMap
	for (unsigned char currentButton = 0; currentButton < 12; currentButton++)
	{
		unsigned char buttonBit = currentButton;
		unsigned char buttonIndex = 0;
		
		// adjust buttonBit and buttonIndex to select position in ButtonState and ButtonMap
		while(buttonBit > 7)
		{
			buttonBit -= 8;
			buttonIndex++;
		}
		
		// if ButtonState is low, add key defined in ButtonMap to Keys
		if (((ButtonState[buttonIndex] >> buttonBit) & 1) == 0)
			Keys[currentButton] = ButtonMap[currentButton];
		else
			Keys[currentButton] = KEY_NONE;
	}
	
	// update Keys currently pressed
	UsbKeyboard_HoldKeys();
}

