Genesis Controller Encoder
Sega Genesis Controller to USB Keyboard Encoder

Written by Robert Lloyd
rl636018@gmail.com


This project allows a Sega Genesis/Mega Drive controller to connect to a PC or other device which accepts USB keyboard input.  The only components required are a microcontroller, DB9 male connector and USB A male connector.  It should be usable with the Ateml AVR microcontrollers below, but has only been tested with an atmega32u2.


Controller Pinout:

DB9	AVR	Function
---	---	--------
1	D0	data
2	D1	data
3	D2	data
4	D3	data
5	+5V	power
6	D4	data
7	C2	select
8	GND	ground
9	D5	data


Compatible with the following AVR microcontrollers:
atmega32u2
atmega32u4
at90usb162
at90usb646
at90usb1286