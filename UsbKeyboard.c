/*
USB Keyboard Example for Teensy USB Development Board

http://www.pjrc.com/teensy/usb_keyboard.html
Copyright (c) 2009 PJRC.COM, LLC
Copyright (c) 2014 David Thompson

Version 1.0: Initial Release
Version 1.1: Add support for Teensy 2.0
Version 1.2: Modified by Dane Gardner for more than 6 simultaneous keys
Version 1.3: Modified by Robert Lloyd to improve formatting, variable names,
			 and general readability.

Compatible with the following devices:
AVR_ATmega32u2
AVR_ATmega32u4
AVR_AT90usb162
AVR_AT90usb646
AVR_AT90usb1286

To configure other devices, see device configuration section in UsbKeyboard.h

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

The Software is provided "as is", without warranty of any kind, express or
implied, including but not limited to the warranties of merchantability,
fitness for a particular purpose and non-infringement. In no event shall the
authors or copyright holders be liable for any claim, damages or other
liability, whether in an action of contract, tort or otherwise, arising from,
out of or in connection with the software or the use or other dealings in
the Software.
*/

#define USB_SERIAL_PRIVATE_INCLUDE
#include "UsbKeyboard.h"

/**************************************************************************
* Configurable Options
**************************************************************************/

// You can change these to name your device.  On Windows, these are
//   only used before an INF file (driver install) is loaded.
#define STR_MANUFACTURER L"Robert Lloyd"
#define STR_PRODUCT L"USB Keyboard Encoder"

// Mac OS-X and Linux automatically load the correct drivers. On
// Windows, even though the driver is supplied by Microsoft, an
// INF file is needed to load the driver. These numbers need to
// match the INF file.
#define VENDOR_ID 0x16C0
#define PRODUCT_ID 0x047E

// USB devices are supposed to implement a halt feature, which is
// rarely (if ever) used. If you comment this line out, the halt
// code will be removed, saving 102 bytes of space (gcc 4.3.0).
// This is not strictly USB compliant, but works with all major
// operating systems.
#define SUPPORT_ENDPOINT_HALT

/**************************************************************************
* Variables (these are the only non-stack RAM usage)
**************************************************************************/

// indicates USB configurations status
// zero when not configured, non-zero when enumerated
static volatile unsigned char UsbConfiguration = 0;

// stores keys to be held
// see UsbKeyboard.h for possible values
// must be sized at 2^n-2 (6, 14, 30, 62, etc.)
// size must match size in UsbKeyboard.h
unsigned char Keys[30] = { 0,0,0,0,0,0,0,0,						   
						   0,0,0,0,0,0,0,0,
						   0,0,0,0,0,0,0,0,
						   0,0,0,0,0,0 };

// stores modifier keys to be held
// see UsbKeyboard.h for possible values
unsigned char ModifierKeys = 0;

// stores Keyboard LED status
// see UsbKeyboard.h for possible values
volatile unsigned char KeyboardLEDs = 0;

// Protocol setting from the host: We use exactly the same report
// either way, so this variable only stores the setting since we
// are required to be able to report which setting is in use.
static unsigned char KeyboardProtocol = 1;

// idle configuration; how often we send the report to the
// host (ms * 4) even when it hasn't changed
static unsigned char KeyboardIdleConfig = 125;

// count until idle timeout
static unsigned char KeyboardIdleCount = 0;

/**************************************************************************
* Endpoint Buffer Configuration
**************************************************************************/

#define KEYBOARD_INTERFACE 0
#define KEYBOARD_ENDPOINT 3
#define KEYBOARD_SIZE (2 + sizeof(Keys))
#define KEYBOARD_BUFFER EP_DOUBLE_BUFFER

#define ENDPOINT0_SIZE KEYBOARD_SIZE

static const unsigned char PROGMEM endpoint_config_table[] = {
  0,
  0,
  1, EP_TYPE_INTERRUPT_IN, EP_SIZE(KEYBOARD_SIZE) | KEYBOARD_BUFFER,
  0
};

/**************************************************************************
* Descriptor Data
**************************************************************************/

// Descriptors are the data that your computer reads when it auto-detects
// this USB device (called "enumeration" in USB lingo). The most commonly
// changed items are editable at the top of this file. Changing things
// in here should only be done by those who've read chapter 9 of the USB
// spec and relevant portions of any USB class specifications!

static const unsigned char PROGMEM device_descriptor[] = {
	18,									// bLength
	1,									// bDescriptorType
	0x00, 0x02,							// bcdUSB
	0,									// bDeviceClass
	0,									// bDeviceSubClass
	0,									// bDeviceProtocol
	ENDPOINT0_SIZE,						// bMaxPacketSize0
	LSB(VENDOR_ID), MSB(VENDOR_ID),		// idVendor
	LSB(PRODUCT_ID), MSB(PRODUCT_ID),	// idProduct
	0x00, 0x01,							// bcdDevice
	1,									// iManufacturer
	2,									// iProduct
	0,									// iSerialNumber
	1									// bNumConfigurations
};

// Keyboard Protocol 1, HID 1.11 spec, Appendix B, page 59-60
static const unsigned char PROGMEM keyboard_hid_report_desc[] = {
	0x05, 0x01,			// Usage Page (Generic Desktop),
	0x09, 0x06,			// Usage (Keyboard),
	0xA1, 0x01,			// Collection (Application),

	0x05, 0x07,			// Usage Page (Key Codes),
	0x19, 0xE0,			// Usage Minimum (224),
	0x29, 0xE7,			// Usage Maximum (231),
	0x15, 0x00,			// Logical Minimum (0),
	0x25, 0x01,			// Logical Maximum (1),
	0x75, 0x01,			// Report Size (1), (1 bit)
	0x95, 0x08,			// Report Count (8), (8 sets of 1 bit)
	0x81, 0x02,			// Input (Data, Variable, Absolute), ;Modifier byte

	0x75, 0x08,			// Report Size (8), (8 bits)
	0x95, 0x01,			// Report Count (1), (1 set of 8 bits)
	0x81, 0x03,			// Input (Constant), ;Reserved byte

	0x05, 0x08,			// Usage Page (LEDs),
	0x19, 0x01,			// Usage Minimum (1),
	0x29, 0x05,			// Usage Maximum (5),
	0x75, 0x01,			// Report Size (1), (1 bit)
	0x95, 0x05,			// Report Count (5), (5 sets of 1 bit)
	0x91, 0x02,			// Output (Data, Variable, Absolute), ;LED report

	0x75, 0x03,			// Report Size (3), (3 bits)
	0x95, 0x01,			// Report Count (1), (1 set of 3 bits)
	0x91, 0x03,			// Output (Constant), ;LED report padding

	0x05, 0x07,			// Usage Page (Key Codes), ;Key array
	0x19, 0x00,			// Usage Minimum (0),
	0x29, 0xFF,			// Usage Maximum (255),
	0x15, 0x00,			// Logical Minimum (0),
	0x25, 0xFF,			// Logical Maximum(255),
	0x75, 0x08,			// Report Size (8), (8 bits)
	0x95, sizeof(Keys),	// Report Count (30), (30 sets of 8 bits) <= Modify for array size
	0x81, 0x00,			// Input (Data, Array),

	0xc0				// End Collection
};

#define CONFIG1_DESC_SIZE (9+9+9+7)
#define KEYBOARD_HID_DESC_OFFSET (9+9)
static const unsigned char PROGMEM config1_descriptor[CONFIG1_DESC_SIZE] = {
	// configuration descriptor, USB spec 9.6.3, page 264-266, Table 9-10
	9,									// bLength;
	2,									// bDescriptorType;
	LSB(CONFIG1_DESC_SIZE),				// wTotalLength
	MSB(CONFIG1_DESC_SIZE),
	1,									// bNumInterfaces
	1,									// bConfigurationValue
	0,									// iConfiguration
	0xC0,								// bmAttributes
	50,									// bMaxPower

	// interface descriptor, USB spec 9.6.5, page 267-269, Table 9-12
	9,									// bLength
	4,									// bDescriptorType
	KEYBOARD_INTERFACE,					// bInterfaceNumber
	0,									// bAlternateSetting
	1,									// bNumEndpoints
	0x03,								// bInterfaceClass (0x03 = HID)
	0x00,								// bInterfaceSubClass (0x01 = Boot)
	0x01,								// bInterfaceProtocol (0x01 = Keyboard)
	0,									// iInterface

	// HID interface descriptor, HID 1.11 spec, section 6.2.1
	9,									// bLength
	0x21,								// bDescriptorType
	0x11, 0x01,							// bcdHID
	0,									// bCountryCode
	1,									// bNumDescriptors
	0x22,								// bDescriptorType
	sizeof(keyboard_hid_report_desc),	// wDescriptorLength
	0,

	// endpoint descriptor, USB spec 9.6.6, page 269-271, Table 9-13
	7,									// bLength
	5,									// bDescriptorType
	KEYBOARD_ENDPOINT | 0x80,			// bEndpointAddress
	0x03,								// bmAttributes (0x03=intr)
	KEYBOARD_SIZE, 0,					// wMaxPacketSize
	1									// bInterval
};

// If you're desperate for a little extra code memory, these strings
// can be completely removed if iManufacturer, iProduct, iSerialNumber
// in the device descriptor are changed to zeros.
struct usb_string_descriptor_struct {
	unsigned char bLength;
	unsigned char bDescriptorType;
	int16_t wString[];
};
static const struct usb_string_descriptor_struct PROGMEM string0 = {
	4,
	3,
	{0x0409}
};
static const struct usb_string_descriptor_struct PROGMEM string1 = {
	sizeof(STR_MANUFACTURER),
	3,
	STR_MANUFACTURER
};
static const struct usb_string_descriptor_struct PROGMEM string2 = {
	sizeof(STR_PRODUCT),
	3,
	STR_PRODUCT
};

// This table defines which descriptor data is sent for each specific
// request from the host (in wValue and wIndex).
static const struct descriptor_list_struct {
	uint16_t wValue;
	uint16_t wIndex;
	const unsigned char *addr;
	unsigned char length;
} PROGMEM descriptor_list[] = {
	{0x0100, 0x0000, device_descriptor, sizeof(device_descriptor)},
	{0x0200, 0x0000, config1_descriptor, sizeof(config1_descriptor)},
	{0x2200, KEYBOARD_INTERFACE, keyboard_hid_report_desc, sizeof(keyboard_hid_report_desc)},
	{0x2100, KEYBOARD_INTERFACE, config1_descriptor+KEYBOARD_HID_DESC_OFFSET, 9},
	{0x0300, 0x0000, (const unsigned char *)&string0, 4},
	{0x0301, 0x0409, (const unsigned char *)&string1, sizeof(STR_MANUFACTURER)},
	{0x0302, 0x0409, (const unsigned char *)&string2, sizeof(STR_PRODUCT)}
};
#define NUM_DESC_LIST (sizeof(descriptor_list)/sizeof(struct descriptor_list_struct))

/**************************************************************************
* Public Functions - these are the API intended for the user
**************************************************************************/

// initialize USB
void InitializeUsb(void)
{
	HW_CONFIG();
	USB_FREEZE(); // enable USB
	PLL_CONFIG(); // config PLL
	while(!(PLLCSR & (1<<PLOCK))) ; // wait for PLL lock
	USB_CONFIG(); // start USB clock
	UDCON = 0; // enable attach resistor
	UsbConfiguration = 0;
	UDIEN = (1<<EORSTE)|(1<<SOFE);
	sei();
}

// return 0 if the USB is not configured, or the configuration
// number selected by the HOST
unsigned char UsbConfigured(void)
{
	return UsbConfiguration;
}

// send the contents of Keys and ModifierKeys
char UsbKeyboard_HoldKeys(void)
{
	unsigned char i, intr_state, timeout;

	if(!UsbConfiguration) return -1;
	intr_state = SREG;
	cli();
	UENUM = KEYBOARD_ENDPOINT;
	timeout = UDFNUML + 50;
	while(1) {
		// are we ready to transmit?
		if(UEINTX & (1<<RWAL)) break;
		SREG = intr_state;
		// has the USB gone offline?
		if(!UsbConfiguration) return -1;
		// have we waited too long?
		if(UDFNUML == timeout) return -1;
		// get ready to try checking again
		intr_state = SREG;
		cli();
		UENUM = KEYBOARD_ENDPOINT;
	}
	UEDATX = ModifierKeys;
	UEDATX = 0;
	for(i = 0; i < sizeof(Keys); i++) {
		UEDATX = Keys[i];
	}
	UEINTX = 0x3A;
	KeyboardIdleCount = 0;
	SREG = intr_state;
	return 0;
}

// perform a single keystroke
char UsbKeystroke(unsigned char key, unsigned char modifier)
{
	char r;

	ModifierKeys = modifier;
	Keys[0] = key;
	r = UsbKeyboard_HoldKeys();
	if(r) return r;
	ModifierKeys = 0;
	Keys[0] = 0;
	return UsbKeyboard_HoldKeys();
}

/**************************************************************************
* Private Functions - not intended as API for the user
**************************************************************************/

// USB Device Interrupt - handle all device-level events
// the transmit buffer flushing is triggered by the start of frame
ISR(USB_GEN_vect)
{
	unsigned char intbits, i;
	static unsigned char div4=0;

	intbits = UDINT;
	UDINT = 0;
	if(intbits & (1<<EORSTI)) {
		UENUM = 0;
		UECONX = 1;
		UECFG0X = EP_TYPE_CONTROL;
		UECFG1X = EP_SIZE(ENDPOINT0_SIZE) | EP_SINGLE_BUFFER;
		UEIENX = (1<<RXSTPE);
		UsbConfiguration = 0;
	}
	if((intbits & (1<<SOFI)) && UsbConfiguration) {
		if(KeyboardIdleConfig && (++div4 & 3) == 0) {
			UENUM = KEYBOARD_ENDPOINT;
			if(UEINTX & (1<<RWAL)) {
				KeyboardIdleCount++;
				if(KeyboardIdleCount == KeyboardIdleConfig) {
					KeyboardIdleCount = 0;
					UEDATX = ModifierKeys;
					UEDATX = 0;
					for(i = 0; i < sizeof(Keys); i++) {
						UEDATX = Keys[i];
					}
					UEINTX = 0x3A;
				}
			}
		}
	}
}

// Misc functions to wait for ready and send/receive packets
static inline void usb_wait_in_ready(void)
{
	while(!(UEINTX & (1<<TXINI))) ;
}
static inline void usb_send_in(void)
{
	UEINTX = ~(1<<TXINI);
}
static inline void usb_wait_receive_out(void)
{
	while(!(UEINTX & (1<<RXOUTI))) ;
}
static inline void usb_ack_out(void)
{
	UEINTX = ~(1<<RXOUTI);
}

// USB Endpoint Interrupt - endpoint 0 is handled here. The
// other endpoints are manipulated by the user-callable
// functions, and the start-of-frame interrupt.
ISR(USB_COM_vect)
{
	unsigned char intbits;
	const unsigned char *list;
	const unsigned char *cfg;
	unsigned char i, n, len, en;
	unsigned char bmRequestType;
	unsigned char bRequest;
	uint16_t wValue;
	uint16_t wIndex;
	uint16_t wLength;
	uint16_t desc_val;
	const unsigned char *desc_addr;
	unsigned char desc_length;

	UENUM = 0;
	intbits = UEINTX;
	if(intbits & (1<<RXSTPI)) {
		bmRequestType = UEDATX;
		bRequest = UEDATX;
		wValue = UEDATX;
		wValue |= (UEDATX << 8);
		wIndex = UEDATX;
		wIndex |= (UEDATX << 8);
		wLength = UEDATX;
		wLength |= (UEDATX << 8);
		UEINTX = ~((1<<RXSTPI) | (1<<RXOUTI) | (1<<TXINI));
		if(bRequest == GET_DESCRIPTOR) {
			list = (const unsigned char *)descriptor_list;
			for(i=0; ; i++) {
				if(i >= NUM_DESC_LIST) {
					UECONX = (1<<STALLRQ)|(1<<EPEN); //stall
					return;
				}
				desc_val = pgm_read_word(list);
				if(desc_val != wValue) {
					list += sizeof(struct descriptor_list_struct);
					continue;
				}
				list += 2;
				desc_val = pgm_read_word(list);
				if(desc_val != wIndex) {
					list += sizeof(struct descriptor_list_struct)-2;
					continue;
				}
				list += 2;
				desc_addr = (const unsigned char *)pgm_read_word(list);
				list += 2;
				desc_length = pgm_read_byte(list);
				break;
			}
			len = (wLength < 256) ? wLength : 255;
			if(len > desc_length) len = desc_length;
			do {
				// wait for host ready for IN packet
				do {
					i = UEINTX;
				} while(!(i & ((1<<TXINI)|(1<<RXOUTI))));
				if(i & (1<<RXOUTI)) return; // abort
				// send IN packet
				n = len < ENDPOINT0_SIZE ? len : ENDPOINT0_SIZE;
				for(i = n; i; i--) {
					UEDATX = pgm_read_byte(desc_addr++);
				}
				len -= n;
				usb_send_in();
			} while(len || n == ENDPOINT0_SIZE);
			return;
		}
		if(bRequest == SET_ADDRESS) {
			usb_send_in();
			usb_wait_in_ready();
			UDADDR = wValue | (1<<ADDEN);
			return;
		}
		if(bRequest == SET_CONFIGURATION && bmRequestType == 0) {
			UsbConfiguration = wValue;
			usb_send_in();
			cfg = endpoint_config_table;
			for(i=1; i<5; i++) {
				UENUM = i;
				en = pgm_read_byte(cfg++);
				UECONX = en;
				if(en) {
					UECFG0X = pgm_read_byte(cfg++);
					UECFG1X = pgm_read_byte(cfg++);
				}
			}
			UERST = 0x1E;
			UERST = 0;
			return;
		}
		if(bRequest == GET_CONFIGURATION && bmRequestType == 0x80) {
			usb_wait_in_ready();
			UEDATX = UsbConfiguration;
			usb_send_in();
			return;
		}

		if(bRequest == GET_STATUS) {
			usb_wait_in_ready();
			i = 0;
			#ifdef SUPPORT_ENDPOINT_HALT
			if(bmRequestType == 0x82) {
				UENUM = wIndex;
				if(UECONX & (1<<STALLRQ)) i = 1;
				UENUM = 0;
			}
			#endif
			UEDATX = i;
			UEDATX = 0;
			usb_send_in();
			return;
		}
		#ifdef SUPPORT_ENDPOINT_HALT
		if((bRequest == CLEAR_FEATURE || bRequest == SET_FEATURE)
			&& bmRequestType == 0x02 && wValue == 0) {
			i = wIndex & 0x7F;
			if(i >= 1 && i <= MAX_ENDPOINT) {
				usb_send_in();
				UENUM = i;
				if(bRequest == SET_FEATURE) {
					UECONX = (1<<STALLRQ)|(1<<EPEN);
					} 
				else {
					UECONX = (1<<STALLRQC)|(1<<RSTDT)|(1<<EPEN);
					UERST = (1 << i);
					UERST = 0;
				}
				return;
			}
		}
		#endif
		if(wIndex == KEYBOARD_INTERFACE) {
			if(bmRequestType == 0xA1) {
				if(bRequest == HID_GET_REPORT) {
					usb_wait_in_ready();
					UEDATX = ModifierKeys;
					UEDATX = 0;
					for(i = 0; i < sizeof(Keys); i++) {
						UEDATX = Keys[i];
					}
					usb_send_in();
					return;
				}
				if(bRequest == HID_GET_IDLE) {
					usb_wait_in_ready();
					UEDATX = KeyboardIdleConfig;
					usb_send_in();
					return;
				}
				if(bRequest == HID_GET_PROTOCOL) {
					usb_wait_in_ready();
					UEDATX = KeyboardProtocol;
					usb_send_in();
					return;
				}
			}
			if(bmRequestType == 0x21) {
				if(bRequest == HID_SET_REPORT) {
					usb_wait_receive_out();
					KeyboardLEDs = UEDATX;
					usb_ack_out();
					usb_send_in();
					return;
				}
				if(bRequest == HID_SET_IDLE) {
					KeyboardIdleConfig = (wValue >> 8);
					KeyboardIdleCount = 0;
					usb_send_in();
					return;
				}
				if(bRequest == HID_SET_PROTOCOL) {
					KeyboardProtocol = wValue;
					usb_send_in();
					return;
				}
			}
		}
	}
	UECONX = (1<<STALLRQ) | (1<<EPEN); // stall
}
